/**
 * @license
 * Copyright (c) 2019 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at
 * http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at
 * http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at
 * http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at
 * http://polymer.github.io/PATENTS.txt
 */

import {LitElement, html, css} from 'lit-element';
import './ejercicios/ejercicio-uno/ejercicio-uno.js';
import './ejercicios/ejercicio-dos/ejercicio-dos.js';
import './ejercicios/ejercicio-tres/ejercicio-tres.js';
import './ejercicios/ejercicio-cuatro/ejercicio-cuatro.js';
import './ejercicios/ejercicio-cinco/ejercicio-cinco.js';
import './ejercicios/ejercicio-seis/ejercicio-seis.js';


/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class MyElement extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        border: solid 1px gray;
        padding: 16px;
        max-width: 800px;
      }
    `;
  }

  static get properties() {
    return {
      options: {type: Array},
      selected: {type: String}
    };
  }

  constructor() {
    super();
    this.options = [
      {
        value: '0',
        text: 'Ejercicio uno'
      },
      {
        value: '1',
        text: 'Ejercicio dos'
      },
      {
        value: '2',
        text: 'Ejercicio tres'
      },
      {
        value: '3',
        text: 'Ejercicio cuatro'
      },
      {
        value: '4',
        text: 'Ejercicio cinco'
      },
      {
        value: '5',
        text: 'Ejercicio seis'
      }
    ];
    this.selected = '0';
  }

  render() {
    return html`
      <select @change="${e => {this.selected = e.target.value;}}">
        ${this.options.map(option => html`
          <option value="${option.value}" ?selected=${this.selected === option.value}>${option.text}</option>
          `)}
      </select>
      <ejercicio-uno ?hidden=${this.selected !== '0'}></ejercicio-uno>
      <ejercicio-dos ?hidden=${this.selected !== '1'}></ejercicio-dos>
      <ejercicio-tres ?hidden=${this.selected !== '2'}></ejercicio-tres>
      <ejercicio-cuatro ?hidden=${this.selected !== '3'}></ejercicio-cuatro>
      <ejercicio-cinco ?hidden=${this.selected !== '4'}></ejercicio-cinco>
      <ejercicio-seis ?hidden=${this.selected !== '5'}></ejercicio-seis>
    `;
  }

}

window.customElements.define('my-element', MyElement);

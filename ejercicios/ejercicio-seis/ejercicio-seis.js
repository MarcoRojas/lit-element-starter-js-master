import { LitElement, html } from 'lit-element';

class EjercicioSeis extends LitElement {
    static get properties() {
        return {
            myArr: { type: Array }
        };
    }

    constructor() {
        super();
        this.myArr = ["foo", "bar"];
    }

    firstUpdated() {
        console.log("first updated!");
    }
    connectedCallback() {
        super.connectedCallback();
        console.log("connected!");
    }

    updated(changedProps) {
        super.updated(changedProps);
        console.log("updated!");
    }

    shouldUpdate() {
        console.log("should update!");
        return true;
    }

    _addItem() {
        this.myArr = [...this.myArr, "baz"];
    }

    render() {
            let { myArr } = this;

            return html `
          <p>Open your console to see when lifecycle methods occur.</p>
          <button @click=${this._addItem}>add item</button>
          <ul>
            ${myArr.map(
              item => html`
                <li>${item}</li>
              `
            )}
          </ul>
        `;
      }
}

customElements.define('ejercicio-seis', EjercicioSeis);
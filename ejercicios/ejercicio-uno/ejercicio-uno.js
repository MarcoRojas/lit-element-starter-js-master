import { LitElement, html } from 'lit-element';

class EjercicioUno extends LitElement {

    static get properties() {
        return {
            number_1: { type: Number },
            number_2: { type: Number }
        };
    }

    constructor() {
        super();
        this.number_1 = 16;
        this.number_2 = 32;
    }

    render() {
        return html `
            <form id="form">
                <input id="amountInput" type="number" name="amount" />
            </form>
        `;
    }

    firstUpdated(changedProperties) {
        this._form = this.shadowRoot.getElementById("form");
        let amountInput = this.amountInput;
        amountInput.focus();
        amountInput.val = this.number_1;
        amountInput.value = this.number_2;
    }

    get amountInput() {
        return this.shadowRoot.getElementById("amountInput");
    }

}

customElements.define('ejercicio-uno', EjercicioUno);
import { LitElement, html } from 'lit-element';

class EjercicioTres extends LitElement {

    static get properties() {
        return {
            names: { type: Array },
            colors: { teyoe: Array }
        };
    }

    constructor() {
        super();
        this.names = ["jorge", "roberto"];
        this.colors = ["rojo", "verde"];
    }

    render() {
            return html `
                <input id="nameInput" name="username" />
                <button @click="${() => this._addName()}">Add name</button>
                <ul>
                    ${this.names.map(name => html`
                        <li>${name}</li>
                    `
                    )}
                </ul>
                <input id="colorInput" name="colorName" />
                <button @click="${() => this._addColor()}">Add color</button>
                <ul>
                    ${this.colors.map(color => html`
                        <li>${color}</li>
                    `
                    )}
                </ul>
            `;
    }
    
    get nameInput() {
        return this.shadowRoot.getElementById("nameInput");
    }
    
    get colorInput() {
        return this.shadowRoot.getElementById("colorInput");
    }
    
    _addName() {
        this.names = [...this.names, this.nameInput.value];
        this.nameInput.value = "";
    }
    
    _addColor() {
        this.colors.push(this.colorInput.value);
        this.colorInput.value = "";
    }
}

customElements.define('ejercicio-tres', EjercicioTres);
import { LitElement, html } from 'lit-element';

class EjercicioCinco extends LitElement {
    static get properties() {
        return {
            messageRequest: { type: String }
        };
    }

    constructor() {
        super();
        this.messageRequest = "Click";
    }

    render() {
        return html `
          <div>
            <p>${this.messageRequest}</p>
            <button @click="${() => this._fetchMessage()}">
              Fetch message
            </button>
          </div>
        `;
    }

    _fetchMessage() {
        this.messageRequest = "Loading...";
        setTimeout(() => (this.messageRequest = "OK"), 2000);
    }
}

customElements.define('ejercicio-cinco', EjercicioCinco);